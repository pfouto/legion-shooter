let path = null;
let nextSquare = null;
let botUp = false;
let botDown = false;
let botLeft = false;
let botRight = false;

let botStopDist = (BLOCK_SIZE - PLAYER_SIZE) / 2 - 1;

function botUpdate() {
    let myPlayer = players[legion.id];


    if (myPlayer.dead) {
        nextSquare = null;
        keyDown({keyCode: 82});
        keyUp({keyCode: 82});
        return;
    }

    if (nextSquare == null) {
        let botTarget = chooseRandomSquare();
        let res = djikstra(botTarget);
        let dist = res[0];
        let prevs = res[1];
        path = [];

        let prevSquare = botTarget;
        for (let i = 0; i <= dist; i++) {
            path.push(prevSquare);
            prevSquare = prevs[[prevSquare[0], prevSquare[1]]];
        }
        path = path.reverse();
        console.log("New target: " + botTarget);
        //console.log("Path:" + path);
        nextSquare = path.shift();
        //console.log("Next square: " + nextSquare);
    }

    let myX = players[legion.id].x;
    let myY = players[legion.id].y;
    let nextX = gridToCenterPixel(nextSquare[1]);
    let nextY = gridToCenterPixel(nextSquare[0]);

    if (distance(myX, myY, nextX, nextY) < BLOCK_SIZE / 3) {
        if (path.length > 0) {
            nextSquare = path.shift();
            //console.log("Next square: " + nextSquare);
        } else {
            nextSquare = null;
            //console.log("Arrived!");
        }
        return
    }

    if (nextX - myX >= botStopDist && !botRight) {
        keyDown({keyCode: 68});
        //console.log("Going right");
        botRight = true;
    } else if (myX > nextX && botRight) {
        keyUp({keyCode: 68});
        //console.log("Stop right");
        botRight = false;
    }

    if (nextY - myY >= botStopDist && !botDown) {
        keyDown({keyCode: 83});
        //console.log("Going down");
        botDown = true;
    } else if (myY > nextY && botDown) {
        keyUp({keyCode: 83});
        //console.log("Stop down");
        botDown = false;
    }

    if (nextX - myX <= -botStopDist && !botLeft) {
        keyDown({keyCode: 65});
        //console.log("Going left");
        botLeft = true;
    } else if (myX < nextX && botLeft) {
        keyUp({keyCode: 65});
        //console.log("Stop left");
        botLeft = false;
    }

    if (nextY - myY <= -botStopDist && !botUp) {
        keyDown({keyCode: 87});
        //console.log("Going up");
        botUp = true;
    } else if (myY < nextY && botUp) {
        keyUp({keyCode: 87});
        //console.log("Stop up");
        botUp = false;
    }

    for (const id in players) {
        if (id === legion.id.toString()) continue;
        if (players[id].dead) continue;

        if (inRectangle(players[id].x, players[id].y,
            myY - PLAYER_SIZE / 2, myY + PLAYER_SIZE / 2, myX - BLOCK_SIZE * 10, myX)) {
            shooting = 'left';
            return;
        }
        if (inRectangle(players[id].x, players[id].y,
            myY - PLAYER_SIZE / 2, myY + PLAYER_SIZE / 2, myX, myX + BLOCK_SIZE * 10)) {
            shooting = 'right';
            return;
        }
        if (inRectangle(players[id].x, players[id].y,
            myY - BLOCK_SIZE * 10, myY, myX - PLAYER_SIZE / 2, myX + PLAYER_SIZE / 2)) {
            shooting = 'up';
            return;
        }
        if (inRectangle(players[id].x, players[id].y,
            myY, myY + BLOCK_SIZE * 10, myX - PLAYER_SIZE / 2, myX + PLAYER_SIZE / 2)) {
            shooting = 'down';
            return;
        }
    }
    shooting = 'none';
}

function inRectangle(x, y, top, bottom, left, right) {
    return x <= right && x >= left && y <= bottom && y >= top;
}

function chooseRandomSquare() {
    let y = 0;
    let x = 0;
    do {
        y = Math.floor(Math.random() * mapHeight);
        x = Math.floor(Math.random() * mapWidth);
    } while (map[y][x] !== 0);
    return [y, x];
}

function getNeighbours(square) {
    let neighbours = [];

    if (map[square[0] + 1][square[1]] === 0) neighbours.push([square[0] + 1, square[1]]);
    if (map[square[0] - 1][square[1]] === 0) neighbours.push([square[0] - 1, square[1]]);
    if (map[square[0]][square[1] + 1] === 0) neighbours.push([square[0], square[1] + 1]);
    if (map[square[0]][square[1] - 1] === 0) neighbours.push([square[0], square[1] - 1]);
    return neighbours;
}

function djikstra(endSquare) {
    //Y, X
    let startSquare = [pixelToGrid(players[legion.id].y), pixelToGrid(players[legion.id].x)];
    console.log("My square: " + startSquare);
    let distances = {};
    // Stores the reference to previous nodes
    let prev = {};
    let pq = new PriorityQueue(mapWidth * mapHeight);
    // Set distances to all nodes to be infinite except startNode
    distances[startSquare] = 0;
    pq.enqueue(startSquare);
    for (let y = 0; y < mapHeight; y++) {
        for (let x = 0; x < mapWidth; x++) {
            if (y !== startSquare[0] || x !== startSquare[1]) {
                distances[[y, x]] = Infinity;
            }
            //prev[[y,x]] = null;
        }
    }
    while (!pq.isEmpty()) {
        let currNode = pq.dequeue();
        getNeighbours(currNode).forEach(neighbor => {
            let alt = distances[currNode] + 1;
            if (alt < distances[neighbor]) {
                distances[neighbor] = alt;
                prev[neighbor] = currNode;
                pq.enqueue(neighbor, distances[neighbor]);
            }
        });
    }
    return [distances[endSquare], prev];
}

class PriorityQueue {
    constructor() {
        this.collection = [];
    }

    enqueue(element) {
        if (this.isEmpty()) {
            this.collection.push(element);
        } else {
            let added = false;
            for (let i = 1; i <= this.collection.length; i++) {
                if (element[1] < this.collection[i - 1][1]) {
                    this.collection.splice(i - 1, 0, element);
                    added = true;
                    break;
                }
            }
            if (!added) {
                this.collection.push(element);
            }
        }
    };

    dequeue() {
        return this.collection.shift();
    };

    isEmpty() {
        return (this.collection.length === 0)
    };
}

function bot() {
    setInterval(function () {
        botUpdate();
    }, 33);
}
