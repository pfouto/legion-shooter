//Joystick code to support mobile browser
if (typeof VirtualJoystick != "undefined") {
    VirtualJoystick.prototype._onTouchStart = function (event) {
        // if there is already a touch inprogress do nothing
        if (this._touchIdx !== null) return;

        // notify event for validation
        var isValid = this.dispatchEvent('touchStartValidation', event);
        if (isValid === false) return;

        // dispatch touchStart
        this.dispatchEvent('touchStart', event);

        // get the first who changed
        var touch = event.changedTouches[0];
        // set the touchIdx of this joystick
        this._touchIdx = touch.identifier;

        // forward the action
        var x = touch.pageX;
        var y = touch.pageY;
        return this._onDown(x, y)
    };
    VirtualJoystick.prototype._onTouchMove = function (event) {
        // if there is no touch in progress, do nothing
        if (this._touchIdx === null) return;

        // try to find our touch event
        var touchList = event.changedTouches;
        for (var i = 0; i < touchList.length && touchList[i].identifier !== this._touchIdx; i++) ;
        // if touch event with the proper identifier isnt found, do nothing
        if (i === touchList.length) return;
        var touch = touchList[i];

        var x = touch.pageX;
        var y = touch.pageY;
        return this._onMove(x, y)
    }
}

let stats = null;
const options = {
    objectServerConnection: {
        type: "NONE"
    }
};

let myName = null;
let js1Id = null;
let js1 = false;
let js2Id = null;
let js2 = false;
let going_left = false;
let going_right = false;
let going_up = false;
let going_down = false;
let shooting_left = false;
let shooting_right = false;
let shooting_up = false;
let shooting_down = false;

while (myName == null || myName === "") {
    myName = prompt("Escolhe um nome (máximo 15 caracteres):", "Player" + randomIntFromInterval(0, 9999));
}

myName = myName.substring(0, 15);

//Starting the legion instance. Options are mostly default. The server IP is the same as the current browser page.
const legion = new Legion(options);
let messageAPI = null;

//Matrix representation of the game map
//0 represents empty block, -1 represents empty blocks where the player can not spawn, everything else represents differently colored walls
const map = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 1],
    [1, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 4, 0, 7, 0, 0, 0, 0, 0, 0, 3, 3, 0, 0, 0, 0, 7, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 5, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 4, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 3, 3, 1],
    [1, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 9, 9, 9, 9, 9, 0, 0, 9, 9, 9, 9, 9, 0, 0, 0, 7, 7, 0, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 9, 0, 9, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 8, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 9,-1,-1,-1,-1, 9, 0, 0, 0, 9, 0, 0, 0, 0, 7, 0, 0, 0, 7, 0, 0, 8, 0, 0, 0, 8, 0, 0, 9,-1, 9,-1, 9, 0, 0, 8, 0, 0, 0, 8, 0, 8, 0, 8, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 9,-1,-1,-1,-1, 9, 0, 0, 0, 9, 0, 0, 0, 0, 7, 0, 7, 7, 7, 0, 0, 8, 8, 0, 0, 8, 0, 9,-1, 9,-1, 9,-1, 9, 0, 8, 0, 0, 0, 8, 0, 8, 0, 8, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 9,-1,-1,-1,-1, 9, 0, 0, 0, 9, 0, 0, 0, 0, 7, 0, 7,-1, 7, 0, 0, 8, 0, 8, 0, 8, 0, 0, 9,-1, 9,-1, 9, 0, 0, 0, 8, 0, 8, 0, 0, 8, 0, 8, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 9,-1,-1,-1,-1, 9, 0, 0, 0, 9, 0, 0, 0, 0, 7, 0, 7, 7, 0, 0, 0, 8, 0, 0, 8, 8, 0, 9,-1, 9,-1, 9,-1, 9, 0, 0, 8, 0, 8, 0, 8, 0, 0, 0, 8, 0, 0, 0, 1],
    [1, 0, 0, 0, 9,-1,-1,-1,-1, 9, 0, 0, 0, 9, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 9,-1, 9,-1, 9, 0, 0, 0, 8, 0, 8, 0, 8, 0, 0, 0, 8, 0, 0, 0, 1],
    [1, 0, 0, 0, 9, 9, 9, 9, 9, 0, 0, 9, 9, 9, 9, 9, 0, 0, 0, 7, 7, 7, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 9, 0, 9, 0, 0, 0, 0, 0, 8, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 1],
    [1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 8, 8, 8, 8, 0, 0, 8, 8, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 8, 8, 8, 0, 0, 0, 0, 8, 8, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 8, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 1],
    [1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 1],
    [1, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 0, 1],
    [1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]
const colors = ["white", "blue", "red", "#73AA05", "#3E64AC", "#57AABC", "#2E773F", "#E56744", "black", "#0080FF"];

const mapHeight = map.length;
const mapWidth = map[0].length;

const BLOCK_SIZE = 24;

const PLAYER_SIZE = BLOCK_SIZE * 0.8;
const BULLET_SIZE = PLAYER_SIZE * 0.6;

const PLAYER_SPEED = 6 * BLOCK_SIZE;
const BULLET_SPEED = 18 * BLOCK_SIZE;

const FOG_OF_WAR = true;

const FOW_DIST = Math.max(mapHeight, mapWidth) * BLOCK_SIZE;

const fow = [];
for (let i = 0; i < mapHeight; i++) {
    fow[i] = [];
    for (let j = 0; j < mapWidth; j++) {
        fow[i][j] = false;
    }
}

const pre_grid = [];
for (let y = 0; y < mapHeight; y += 1) {
    pre_grid[y] = gridToCenterPixel(y);
}
for (let x = 0; x < mapWidth; x += 1) {
    pre_grid[x] = gridToCenterPixel(x);
}

function valid(y, x) {
    return map[y] != null && map[y][x] != null;
}

const tl = [];
const tr = [];
const bl = [];
const br = [];
const tlf = [];
const trf = [];
const blf = [];
const brf = [];
const pre_comp_draw = function () {
    for (let y = 0; y < mapHeight; y += 1) {
        tl[y] = [];
        tr[y] = [];
        bl[y] = [];
        br[y] = [];
        tlf[y] = [];
        trf[y] = [];
        blf[y] = [];
        brf[y] = [];
        for (let x = 0; x < mapWidth; x += 1) {
            tl[y][x] = ((valid(y - 1, x) && map[y - 1][x] > 0) || (valid(y, x - 1) && map[y][x - 1] > 0)) ? 0 : 8;
            tr[y][x] = ((valid(y - 1, x) && map[y - 1][x] > 0) || (valid(y, x + 1) && map[y][x + 1] > 0)) ? 0 : 8;
            bl[y][x] = ((valid(y + 1, x) && map[y + 1][x] > 0) || (valid(y, x - 1) && map[y][x - 1] > 0)) ? 0 : 8;
            br[y][x] = ((valid(y + 1, x) && map[y + 1][x] > 0) || (valid(y, x + 1) && map[y][x + 1] > 0)) ? 0 : 8;
            tlf[y][x] = ((valid(y - 1, x) && fow[y - 1][x]) || (valid(y, x - 1) && fow[y][x - 1])) ? 1 : 0;
            trf[y][x] = ((valid(y - 1, x) && fow[y - 1][x]) || (valid(y, x + 1) && fow[y][x + 1])) ? 1 : 0;
            blf[y][x] = ((valid(y + 1, x) && fow[y + 1][x]) || (valid(y, x - 1) && fow[y][x - 1])) ? 1 : 0;
            brf[y][x] = ((valid(y + 1, x) && fow[y + 1][x]) || (valid(y, x + 1) && fow[y][x + 1])) ? 1 : 0;
        }
    }
};
pre_comp_draw();

let frameNumber = 0;

const players = {};
const bullets = [];
let shooting = 'none';
let lastShot = 0;
const RELOAD_TIME = 500;

let SHOW_CONTROLS = true;

let scoreBoard = false;
let helpOverlay = false;

//Renderer
const canvas = document.getElementById("canvas");
const pixelWidth = mapWidth * BLOCK_SIZE;    //canvas.width;
const pixelHeight = mapHeight * BLOCK_SIZE;      //canvas.height;

canvas.setAttribute("width", pixelWidth + "px");
canvas.setAttribute("height", pixelHeight + "px");

const ctx = canvas.getContext("2d");
let lastRender = 0;

//Helpers
const movementKeyMap = {
    68: 'right',
    65: 'left',
    87: 'up',
    83: 'down'
};

const shootKeyMap = {
    37: 'left',
    38: 'up',
    39: 'right',
    40: 'down'
};

const groupOptions = {
    id: "2d_game",
    secret: "default"
};

//Legion callback for when a change occurs in the Legion overlay (e.g. a new user joins the overlay)
legion.overlay.setOnChange(function (change, peerIds, serverIds) {
    console.log("Change: " + peerIds, serverIds);
    const network = $("#network_status");
    const pc = $("#network_status_peerCount")[0];
    const sc = $("#network_status_server")[0];
    const legionIdText = $("#legion_id_status")[0];
    if (legionIdText)
        legionIdText.innerText = legion.id;
    if (sc)
        if (serverIds.length > 0) {
            sc.innerText = "Connected to [" + serverIds + "].";
        } else {
            sc.innerText = "Disconnected.";
        }
    if (pc)
        if (peerIds.length === 0) {
            pc.innerText = "No peers.";
        } else {
            pc.innerText = "(" + peerIds.length + ") " + peerIds;
        }
    if (serverIds.length === 0) {
        network.removeClass("alert-success");
        network.addClass("alert-danger");
    } else {
        network.removeClass("alert-danger");
        network.addClass("alert-success");
    }
});

let firstJoin = true;

//Legion callback for when I join the overlay
legion.joinGroup(groupOptions,
    function (group) {
        if (firstJoin) {
            firstJoin = false;
            //This game only uses the message API, and no CRDTs
            messageAPI = group.getMessageAPI();
            //Registering handlers for every message:
            messageAPI.setHandlerFor("Join", handleJoinMessage);
            messageAPI.setHandlerFor("Spawn", handleSpawnMessage);
            messageAPI.setHandlerFor("Movement", handleMovementMessage);
            messageAPI.setHandlerFor("Shoot", handleShootMessage);
            messageAPI.setHandlerFor("FullUpdate", handleFullUpdateMessage);
            messageAPI.setHandlerFor("Kill", handleKillMessage);
            messageAPI.setHandlerFor("Leaving", handleLeavingMessage);
            window.addEventListener("keydown", keyDown, false);
            window.addEventListener("keyup", keyUp, false);
            //Inform other players that I'm joining the game
            messageAPI.broadcast("Join", {id: legion.id, name: myName});
            console.log(legion.id);
            window.requestAnimationFrame(loop);
        }

    },
    function (server, data) {
    },
    function (failMessage) {
        console.error("failed to join");
        console.error(failMessage)
    }
);

//Message handlers. Mostly change local players state

//When a player kills another
function handleKillMessage(message) {
    const data = message.data;
    if (players[data.killedId]) {
        players[data.killedId].dead = true;
        players[data.killedId].currentScore = 0;
    }
    players[data.killerId].totalScore = data.totalScore;
    players[data.killerId].currentScore = data.currentScore;
    if (data.killedId == legion.id) {
        shooting = 'none';
    }

}

//When a player leaves the game
function handleLeavingMessage(message) {
    const data = message.data;
    delete players[data.id];
}

//Each player send a full update of its own state to each joining player
function handleFullUpdateMessage(message) {
    const data = message.data;
    if (!players[data.id])
        players[data.id] = {};
    players[data.id].x = data.x;
    players[data.id].name = data.name;
    players[data.id].y = data.y;
    players[data.id].left = data.left;
    players[data.id].up = data.up;
    players[data.id].right = data.right;
    players[data.id].down = data.down;
    players[data.id].dead = data.dead;
    players[data.id].totalScore = data.totalScore;
    players[data.id].currentScore = data.currentScore;
}

//When a player shoots
function handleShootMessage(message) {
    const data = message.data;
    const newBullet = {
        x: data.x,
        y: data.y,
        direction: data.direction,
        id: data.id
    };
    bullets.push(newBullet);
}

//When a player (re-)spawns after dying
function handleSpawnMessage(message) {
    const data = message.data;
    players[data.id].x = data.x;
    players[data.id].y = data.y;
    players[data.id].dead = false;
}

//When a player joins the game
function handleJoinMessage(message) {
    const data = message.data;
    console.error("Joining: " + data.id + " " + data.name);
    if (!players[data.id]) {
        players[data.id] = {
            name: data.name,
            x: 0,
            y: 0,
            left: false,
            right: false,
            up: false,
            down: false,
            dead: true,
            totalScore: 0,
            currentScore: 0
        };
    }

    if (data.id !== legion.id) {
        messageAPI.broadcast("FullUpdate", {
            x: players[legion.id].x,
            y: players[legion.id].y,
            id: legion.id,
            name: myName,
            left: players[legion.id].left,
            right: players[legion.id].right,
            up: players[legion.id].up,
            down: players[legion.id].down,
            dead: players[legion.id].dead,
            totalScore: players[legion.id].totalScore,
            currentScore: players[legion.id].currentScore
        });
    }
}

//When a player moves
function handleMovementMessage(message) {
    const data = message.data;
    if (!players[data.id])
        players[data.id] = {};
    players[data.id].x = data.x;
    players[data.id].y = data.y;
    if (data.left !== undefined) {
        players[data.id].left = data.left;
    }
    if (data.up !== undefined) {
        players[data.id].up = data.up;
    }
    if (data.right !== undefined) {
        players[data.id].right = data.right;
    }
    if (data.down !== undefined) {
        players[data.id].down = data.down;
    }
}

//Main loop
function loop(timestamp) {
    const progress = timestamp - lastRender;

    //Update game state
    update(progress, timestamp);
    //Draw the game
    draw();
    input_loop(progress);

    lastRender = timestamp;

    if (stats) {
        stats.update();
    } else {
        stats = new Stats();
        document.getElementById("statsDiv").appendChild(stats.dom);
    }
    window.requestAnimationFrame(loop);
}

function update(progress, timestamp) {
    const delta = progress / 1000;

    //Update FOW
    frameNumber++;
    if (frameNumber % 5 === 0 && FOG_OF_WAR) {
        if (!players[legion.id].dead) {
            calcFow();
            pre_comp_draw();
        }
        frameNumber = 0;
    }

    //Shoot
    if (shooting !== 'none' && timestamp > (lastShot + RELOAD_TIME)) {
        lastShot = timestamp;
        //Inform other players that I shot a bullet
        messageAPI.broadcast("Shoot", {
            x: players[legion.id].x,
            y: players[legion.id].y,
            direction: shooting,
            id: legion.id
        });
    }

    //Update players
    let totalStep = delta * PLAYER_SPEED;
    while (totalStep > 0) {
        let step = Math.min(totalStep, BLOCK_SIZE * 0.25);
        totalStep -= step;
        for (const id in players) {
            if (!players[id].dead) {

                if (players[id].left) {
                    players[id].x -= step;
                    if (isInsideWall(players[id].y - PLAYER_SIZE / 2, players[id].x - PLAYER_SIZE / 2) || //top left
                        isInsideWall(players[id].y + PLAYER_SIZE / 2 - 1, players[id].x - PLAYER_SIZE / 2)) { //bottom left
                        players[id].x = (pixelToGrid(players[id].x)) * BLOCK_SIZE + PLAYER_SIZE / 2;
                    }
                } else if (players[id].right) {
                    players[id].x += step;
                    if (isInsideWall(players[id].y - PLAYER_SIZE / 2, players[id].x + PLAYER_SIZE / 2 - 1) || //top right
                        isInsideWall(players[id].y + PLAYER_SIZE / 2 - 1, players[id].x + PLAYER_SIZE / 2 - 1)) { //bottom right
                        players[id].x = (pixelToGrid(players[id].x) + 1) * BLOCK_SIZE - PLAYER_SIZE / 2;
                    }
                }

                if (players[id].up) {
                    players[id].y -= step;
                    if (isInsideWall(players[id].y - PLAYER_SIZE / 2, players[id].x - PLAYER_SIZE / 2) || //top left
                        isInsideWall(players[id].y - PLAYER_SIZE / 2, players[id].x + PLAYER_SIZE / 2 - 1)) { //top right
                        players[id].y = (pixelToGrid(players[id].y)) * BLOCK_SIZE + PLAYER_SIZE / 2;
                    }
                } else if (players[id].down) {
                    players[id].y += step;
                    if (isInsideWall(players[id].y + PLAYER_SIZE / 2 - 1, players[id].x - PLAYER_SIZE / 2) || //bottom left
                        isInsideWall(players[id].y + PLAYER_SIZE / 2 - 1, players[id].x + PLAYER_SIZE / 2 - 1)) { //bottom right
                        players[id].y = (pixelToGrid(players[id].y) + 1) * BLOCK_SIZE - PLAYER_SIZE / 2;
                    }
                }
            }

        }

        //Update bullets
        step = delta * BULLET_SPEED;
        for (let i = 0; i < bullets.length; i++) {
            //Move
            bullets[i].x += bullets[i].direction === 'left' ? -step : bullets[i].direction === 'right' ? step : 0;
            bullets[i].y += bullets[i].direction === 'up' ? -step : bullets[i].direction === 'down' ? step : 0;

            //Check wall collision
            if (isInsideWall(bullets[i].y, bullets[i].x)) {
                bullets.splice(i, 1);
                i--;
                continue;
            }

            //Check end-of-map
            if (bullets[i].x > pixelWidth || bullets[i].x < 0 || bullets[i].y < 0 || bullets[i].y > pixelHeight) {
                bullets.splice(i, 1);
                i--;
                continue;
            }

            //Check player collision
            if (bullets[i].id === legion.id) {
                for (const id2 in players) {
                    //noinspection EqualityComparisonWithCoercionJS
                    if (!players[id2].dead && id2 != bullets[i].id && intersects(players[id2].x, players[id2].y,
                        bullets[i].x, bullets[i].y)) {
                        //Inform other players that I killed someone
                        messageAPI.broadcast("Kill",
                            {
                                killerName: myName,
                                killerId: legion.id,
                                killedId: id2,
                                totalScore: players[legion.id].totalScore + 1,
                                currentScore: players[legion.id].currentScore + 1
                            });
                        console.log("kill: " + id2);
                    }
                }
            }
        }
    }
}

function calcFow() {
    for (let y = 0; y < mapHeight; y += 1) {
        for (let x = 0; x < mapWidth; x += 1) {
            if (map[y][x] <= 0) {
                fow[y][x] = pointInFow(players[legion.id].x, players[legion.id].y, pre_grid[x], pre_grid[y]);
            } else {
                fow[y][x] = true;
            }
        }
    }
}

function draw() {

    //BG
    ctx.fillStyle = "#AAAAAA";
    ctx.fillRect(0, 0, pixelWidth, pixelHeight);

    //Walls
    for (let y = 0; y < mapHeight; y += 1) {
        for (let x = 0; x < mapWidth; x += 1) {
            if (map[y][x] > 0) {
                ctx.fillStyle = colors[map[y][x]];
                roundRect(ctx, x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE,
                    {tl: tl[y][x], tr: tr[y][x], br: br[y][x], bl: bl[y][x]},
                    {tl: tlf[y][x], tr: trf[y][x], br: brf[y][x], bl: blf[y][x]});
            } else {
                if (!players[legion.id].dead && fow[y][x]) {
                    //ctx.fillStyle = "grey";
                    //ctx.fillRect(x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
                } else {
                    ctx.fillStyle = "white";
                    ctx.fillRect(x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
                }
            }
        }
    }

    //Bullets
    ctx.fillStyle = "purple";
    for (let i = 0; i < bullets.length; i++) {

        if (pixelToGrid(bullets[i].y) >= mapHeight || pixelToGrid(bullets[i].x >= mapWidth) ||
            pixelToGrid(bullets[i].y) < 0 || pixelToGrid(bullets[i].x < 0)) {
            continue;
        }

        if (players[legion.id].dead || !fow[pixelToGrid(bullets[i].y)][pixelToGrid(bullets[i].x)]) {
            if (bullets[i].id === legion.id) {
                ctx.fillStyle = "green";
            } else {
                ctx.fillStyle = "red";
            }
            roundRect(ctx, bullets[i].x - (BULLET_SIZE / 2), bullets[i].y - (BULLET_SIZE / 2),
                BULLET_SIZE, BULLET_SIZE, BULLET_SIZE * 0.3);
        }
    }

    //Players
    ctx.font = "bold 14px Courier New";
    ctx.lineWidth = 2;
    for (const id in players) {
        if (pixelToGrid(players[id].y) >= mapHeight || pixelToGrid(players[id].x >= mapWidth) ||
            pixelToGrid(players[id].y) < 0 || pixelToGrid(players[id].x < 0)) {
            continue;
        }
        if (players[id] && (!players[id].dead && (players[legion.id].dead ||
            !fow[pixelToGrid(players[id].y)][pixelToGrid(players[id].x)]))) {

            ctx.fillStyle = (id === legion.id.toString()) ? 'green' : 'red';
            roundRect(ctx, players[id].x - (PLAYER_SIZE / 2), players[id].y - (PLAYER_SIZE / 2),
                PLAYER_SIZE, PLAYER_SIZE, PLAYER_SIZE * 0.3);
            const nameText = players[id].name; //players[id].name + " " + players[id].currentScore + " (" + players[id].totalScore + ")";
            const textWidth = ctx.measureText(nameText).width;
            ctx.fillText(nameText, players[id].x - textWidth / 2, players[id].y - PLAYER_SIZE);
        }
    }

    //GUI
    if (players[legion.id].dead && SHOW_CONTROLS) {
        ctx.fillStyle = "yellow";
        ctx.font = "20px Verdana";
        const startText = "R: Iniciar o jogo.";
        const startText2 = "H: Ver os controlos.";
        const startText3 = "Tab: Ver as pontuações.";
        const startText4 = "F5: Mudar o nome.";

        ctx.fillStyle = "#12221a";
        ctx.globalAlpha = 0.8;
        const maxWidth = Math.max(ctx.measureText(startText).width, ctx.measureText(startText2).width,
            ctx.measureText(startText3).width, ctx.measureText(startText4).width);
        roundRect(ctx, BLOCK_SIZE * 2 - 10, BLOCK_SIZE + 3, maxWidth + 20, 130, 10);
        ctx.fillStyle = "yellow";
        ctx.fillText(startText, BLOCK_SIZE * 2, BLOCK_SIZE * 2 + 10);
        ctx.fillText(startText2, BLOCK_SIZE * 2, BLOCK_SIZE * 2 + 40);
        ctx.fillText(startText3, BLOCK_SIZE * 2, BLOCK_SIZE * 2 + 70);
        ctx.fillText(startText4, BLOCK_SIZE * 2, BLOCK_SIZE * 2 + 100);
        ctx.globalAlpha = 1;
    }

    if (scoreBoard) {
        ctx.font = "20px Verdana";
        const sortable = [];
        for (const id in players) {
            sortable.push([players[id].name, players[id].totalScore]);
        }
        sortable.sort(function (a, b) {
            return b[1] - a[1];
        });

        const scoreTexts = [];
        let scoreTextWidth = 0;
        for (let i = 0; i < sortable.length; i++) {
            const scoreText = (i + 1) + ". " + sortable[i][0] + ":\t " + sortable[i][1];
            scoreTexts.push([sortable[i][0], scoreText]);
            scoreTextWidth = Math.max(scoreTextWidth, ctx.measureText(scoreText).width);
        }
        ctx.fillStyle = "#12221a";
        ctx.globalAlpha = 0.9;
        roundRect(ctx, pixelWidth / 2 - scoreTextWidth / 2 - 5, pixelHeight * 0.1,
            scoreTextWidth + 10, 20 + 23 * scoreTexts.length + 15, 10);
        ctx.globalAlpha = 1;

        let pos = 1;
        ctx.fillStyle = "yellow";
        ctx.fillText("Score:", pixelWidth / 2 - scoreTextWidth / 2, pixelHeight * 0.1 + 20);
        for (let i = 0; i < scoreTexts.length; i++) {
            if (scoreTexts[i][0] === myName) {
                ctx.fillStyle = "green";
            } else {
                ctx.fillStyle = "#d2d3d5";
            }
            ctx.fillText(scoreTexts[i][1], pixelWidth / 2 - scoreTextWidth / 2, pixelHeight * 0.1 + 20 + 23 * pos);
            pos++;
        }
    }

    if (helpOverlay) {

        const helpText =
            ["Controlos:",
                "Utiliza as teclas W, A, S e D para te movimentares.",
                "Utiliza as setas de direção para disparares."];

        ctx.font = "20px Verdana";
        let maxHelpWidth = 0;
        for (let i = 0; i < helpText.length; i++) {
            maxHelpWidth = Math.max(maxHelpWidth, ctx.measureText(helpText[i]).width);
        }

        ctx.fillStyle = "#12221a";
        ctx.globalAlpha = 0.8;
        roundRect(ctx, BLOCK_SIZE * 2 - 10, BLOCK_SIZE * 2 + 130, maxHelpWidth + 20, 10 + 23 * (helpText.length), 10);
        ctx.globalAlpha = 1;

        ctx.fillStyle = "yellow";
        for (let i = 0; i < helpText.length; i++) {
            ctx.fillText(helpText[i], BLOCK_SIZE * 2, BLOCK_SIZE * 2 + 130 + 23 * (i + 1));
        }
    }

}

const defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};

function roundRect(ctx, x, y, width, height, radius, cornerColor) {
    if (typeof radius === 'number') {
        radius = {tl: radius, tr: radius, br: radius, bl: radius};
    } else {
        for (const side in defaultRadius) {
            radius[side] = radius[side] || defaultRadius[side];
        }
    }

    if (cornerColor != null) {
        const oldColor = ctx.fillStyle;
        ctx.fillStyle = "white";
        if (radius.tl > 0 && cornerColor.tl === 0) {
            ctx.fillRect(x, y, width / 2, height / 2);
        }
        if (radius.tr > 0 && cornerColor.tr === 0) {
            ctx.fillRect(x + width / 2, y, width / 2, height / 2);
        }
        if (radius.bl > 0 && cornerColor.bl === 0) {
            ctx.fillRect(x, y + height / 2, width / 2, height / 2);
        }
        if (radius.br > 0 && cornerColor.br === 0) {
            ctx.fillRect(x + width / 2, y + height / 2, width / 2, height / 2);
        }
        ctx.fillStyle = oldColor;
    }

    ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    ctx.closePath();
    ctx.fill();

}

//Helper funcs - map
function pointInFow(playerX, playerY, pointX, pointY) {

    const dist = distance(playerX, playerY, pointX, pointY);
    //Check dist
    if (dist > FOW_DIST)
        return true;

    //Check if blocked
    let blendStep = BLOCK_SIZE / dist;
    blendStep = blendStep / 10;

    for (let blend = blendStep; blend < 1.0; blend += blendStep) {
        const pX = playerX + (pointX - playerX) * blend;
        const pY = playerY + (pointY - playerY) * blend;
        if (isInsideWall(pY, pX)) {
            return true;
        }
    }
    return false;
}

function isInsideWall(pixelY, pixelX) {
    const gridX = pixelToGrid(pixelX);
    const gridY = pixelToGrid(pixelY);
    if (gridX < 0 || gridX >= mapWidth || gridY < 0 || gridY >= mapHeight)
        return false;
    return map[gridY][gridX] > 0;
}

function pixelToGrid(cord) {
    return Math.floor(cord / BLOCK_SIZE);
}

function gridToCenterPixel(cord) {
    return cord * BLOCK_SIZE + BLOCK_SIZE / 2;
}

//Helper funcs - general
function intersects(playerX, playerY, bulletX, bulletY) {
    const pL = playerX - (PLAYER_SIZE / 2);
    const pR = playerX + (PLAYER_SIZE / 2);
    const pT = playerY - (PLAYER_SIZE / 2);
    const pB = playerY + (PLAYER_SIZE / 2);
    const bL = bulletX - (BULLET_SIZE / 2);
    const bR = bulletX + (BULLET_SIZE / 2);
    const bT = bulletY - (BULLET_SIZE / 2);
    const bB = bulletY + (BULLET_SIZE / 2);

    return !(bL > pR ||
        bR < pL ||
        bT > pB ||
        bB < pT);

}

function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function distance(x1, y1, x2, y2) {
    const a = x1 - x2;
    const b = y1 - y2;
    return Math.sqrt(a * a + b * b);
}

//Input
function keyDown(event) {
    if (event.keyCode === 9) { //Tab
        scoreBoard = true;
        if (event.preventDefault) {
            event.preventDefault();
            event.stopPropagation();
        }
        return;
    }

   if (event.keyCode === 76) { //H
        SHOW_CONTROLS = !SHOW_CONTROLS;
        if (event.preventDefault) {
            event.preventDefault();
            event.stopPropagation();
        }
        return;
    }

    if (event.keyCode === 72) { //H
        helpOverlay = true;
        if (event.preventDefault) {
            event.preventDefault();
            event.stopPropagation();
        }
        return;
    }

    if (event.keyCode === 82) {//Respawn (R)
        if (players[legion.id].dead) {
            let x = 0, y = 0;
            do {
                x = randomIntFromInterval(0, mapWidth - 1);
                y = randomIntFromInterval(0, mapHeight - 1);
            } while (map[y][x] !== 0);
            //Inform other players that I spawned in a certain location
            messageAPI.broadcast("Spawn", {
                x: x * BLOCK_SIZE + BLOCK_SIZE / 2,
                y: y * BLOCK_SIZE + BLOCK_SIZE / 2,
                id: legion.id
            });
        }
        if (event.preventDefault) {
            event.preventDefault();
            event.stopPropagation();
        }
        return;
    }

    //Movement
    let key = movementKeyMap[event.keyCode];
    if (key) {
        switch (key) {
            //Inform other players that I'm starting to move in a certain direction
            case "left":
                if (!players[legion.id].left) {
                    messageAPI.broadcast("Movement", {
                        x: players[legion.id].x,
                        y: players[legion.id].y,
                        left: true,
                        id: legion.id
                    });
                }
                break;
            case "right":
                if (!players[legion.id].right) {
                    messageAPI.broadcast("Movement", {
                        x: players[legion.id].x,
                        y: players[legion.id].y,
                        right: true,
                        id: legion.id
                    });
                }
                break;
            case "up":
                if (!players[legion.id].up) {
                    messageAPI.broadcast("Movement", {
                        x: players[legion.id].x,
                        y: players[legion.id].y,
                        up: true,
                        id: legion.id
                    });
                }
                break;
            case "down":
                if (!players[legion.id].down) {
                    messageAPI.broadcast("Movement", {
                        x: players[legion.id].x,
                        y: players[legion.id].y,
                        down: true,
                        id: legion.id
                    });
                }
                break;
        }
        if (event.preventDefault) {
            event.preventDefault();
            event.stopPropagation();
        }
        return;
    }

    //Shooting
    key = shootKeyMap[event.keyCode];
    if (key) {
        if (!players[legion.id].dead) {
            shooting = key;
        }
        if (event.preventDefault) {
            event.preventDefault();
            event.stopPropagation();
        }
    }
}

function keyUp(event) {
    if (event.keyCode === 9) { //Tab
        scoreBoard = false;
        return;
    }
    if (event.keyCode === 72) { //H
        helpOverlay = false;
        return;
    }

    let key = movementKeyMap[event.keyCode];
    if (key) {
        switch (key) {
            //Inform other players that I stopped moving in a certain direction
            case "left":
                messageAPI.broadcast("Movement", {
                    x: players[legion.id].x,
                    y: players[legion.id].y,
                    left: false,
                    id: legion.id
                });
                break;
            case "right":
                messageAPI.broadcast("Movement", {
                    x: players[legion.id].x,
                    y: players[legion.id].y,
                    right: false,
                    id: legion.id
                });
                break;
            case "up":
                messageAPI.broadcast("Movement", {
                    x: players[legion.id].x,
                    y: players[legion.id].y,
                    up: false,
                    id: legion.id
                });
                break;
            case "down":
                messageAPI.broadcast("Movement", {
                    x: players[legion.id].x,
                    y: players[legion.id].y,
                    down: false,
                    id: legion.id
                });
                break;
        }
    }

    key = shootKeyMap[event.keyCode];
    if (key) {

        if (shooting === key) {
            shooting = 'none';
        }
    }
}

//Inform other players when I'm leaving the game (closing the browser window)
window.onbeforeunload = function () {
    messageAPI.broadcast("Leaving", {id: legion.id});
};

// ***************** Joystick events ***************************
const joystick1 = new VirtualJoystick({
    container: document.body,
    strokeStyle: 'cyan',
    limitStickTravel: true,
    stickRadius: 150
});

joystick1.addEventListener('touchStartValidation', function (event) {
    const touch = event.changedTouches[0];
    return touch.pageX <= window.innerWidth / 2;
});

// one on the right of the screen
const joystick2 = new VirtualJoystick({
    container: document.body,
    strokeStyle: 'orange',
    limitStickTravel: true,
    stickRadius: 150
});

joystick2.addEventListener('touchStartValidation', function (event) {
    const touch = event.changedTouches[0];
    return touch.pageX > window.innerWidth / 2;
});


joystick1.addEventListener('touchStart', function (event) {
    if (players[legion.id].dead) keyDown({keyCode: 82});
    if (js1Id !== null) return;
    js1 = true;
    const touch = event.changedTouches[0];
    js1Id = touch.identifier;
});

joystick1.addEventListener('touchEnd', function (event) {
    if (js1Id === null) return;
    const touchList = event.changedTouches;
    for (let i = 0; i < touchList.length; i++) {
        if (touchList[i].identifier === js1Id) {
            // reset touchIdx - mark it as no-touch-in-progress
            js1Id = null;
            js1 = false;
        }
    }
});

joystick2.addEventListener('touchStart', function (event) {
    if (players[legion.id].dead) keyDown({keyCode: 82});
    if (js2Id !== null) return;
    js2 = true;
    const touch = event.changedTouches[0];
    js2Id = touch.identifier;
});

joystick2.addEventListener('touchEnd', function (event) {
    if (js2Id === null) return;
    const touchList = event.changedTouches;
    for (let i = 0; i < touchList.length; i++) {
        if (touchList[i].identifier === js2Id) {
            // reset touchIdx - mark it as no-touch-in-progress
            js2Id = null;
            js2 = false;
        }
    }
});

function input_loop() {
    if (js1) {
        if (joystick1.right() && !going_right) {
            keyDown({keyCode: 68});
            going_right = true;
        }
        if (joystick1.left() && !going_left) {
            keyDown({keyCode: 65});
            going_left = true;
        }
        if (joystick1.up() && !going_up) {
            keyDown({keyCode: 87});
            going_up = true;
        }
        if (joystick1.down() && !going_down) {
            keyDown({keyCode: 83});
            going_down = true;
        }
        if (!joystick1.right() && going_right) {
            keyUp({keyCode: 68});
            going_right = false;
        }
        if (!joystick1.left() && going_left) {
            keyUp({keyCode: 65});
            going_left = false;
        }
        if (!joystick1.up() && going_up) {
            keyUp({keyCode: 87});
            going_up = false;
        }
        if (!joystick1.down() && going_down) {
            keyUp({keyCode: 83});
            going_down = false;
        }
    } else {
        if (going_down) {
            keyUp({keyCode: 83});
            going_down = false;
        }
        if (going_up) {
            keyUp({keyCode: 87});
            going_up = false;
        }
        if (going_left) {
            keyUp({keyCode: 65});
            going_left = false;
        }
        if (going_right) {
            keyUp({keyCode: 68});
            going_right = false;
        }
    }

    if (js2) {
        if (joystick2.right() && !shooting_right) {
            shooting_right = shooting_left = shooting_down = shooting_up = false;
            shooting_right = true;
            keyDown({keyCode: 39});
        }
        if (joystick2.left() && !shooting_left) {
            shooting_right = shooting_left = shooting_down = shooting_up = false;
            shooting_left = true;
            keyDown({keyCode: 37});
        }
        if (joystick2.up() && !shooting_up) {
            shooting_right = shooting_left = shooting_down = shooting_up = false;
            shooting_up = true;
            keyDown({keyCode: 38});
        }
        if (joystick2.down() && !shooting_down) {
            shooting_right = shooting_left = shooting_down = shooting_up = false;
            shooting_down = true;
            keyDown({keyCode: 40});
        }
        if (!joystick2.right() && shooting_right) {
            shooting_right = true;
            keyUp({keyCode: 39});
        }
        if (!joystick2.left() && shooting_left) {
            shooting_left = true;
            keyUp({keyCode: 37});
        }
        if (!joystick2.up() && shooting_up) {
            shooting_up = true;
            keyUp({keyCode: 38});
        }
        if (!joystick2.down() && shooting_down) {
            shooting_down = true;
            keyUp({keyCode: 40});
        }
    } else {
        if (shooting_right) {
            keyUp({keyCode: 39});
            shooting_right = false;
        }
        if (shooting_left) {
            keyUp({keyCode: 37});
            shooting_left = false;
        }
        if (shooting_up) {
            keyUp({keyCode: 38});
            shooting_up = false;
        }
        if (shooting_down) {
            keyUp({keyCode: 40});
            shooting_down = false;
        }
    }
}
