var options = {
    objectServerConnection: {
        type: "NONE"
    }
};

var groupOptions = {
    id: "2d_game",
    secret: "default"
};

var legion = new Legion(options);
var messageAPI = null;

//Game

var players = {};

//Renderer
var canvas = document.getElementById("canvas");

var ctx = canvas.getContext("2d");

//Init
legion.joinGroup(groupOptions,
    function (group) {
        messageAPI = group.getMessageAPI();
        messageAPI.setHandlerFor("Join", handleJoinMessage);
        messageAPI.setHandlerFor("FullUpdate", handleFullUpdateMessage);
        messageAPI.setHandlerFor("Kill", handleKillMessage);
        messageAPI.setHandlerFor("Leaving", handleLeavingMessage);
        draw();
    },
    function (server, data) {
    },
    function (failMessage) {
        console.error("failed to join");
        console.error(failMessage)
    }
);

//Message handlers
function handleKillMessage(message) {
    var data = message.data;
    if (!players[data.killerId])
        players[data.killerId] = {};
    players[data.killerId].totalScore = data.totalScore;
    players[data.killerId].name = data.killerName;
    draw();
}

function handleLeavingMessage(message) {
    var data = message.data;
    if (players[data.id])
        delete players[data.id];
    draw();
}

function handleFullUpdateMessage(message) {
    var data = message.data;
    if (!players[data.id])
        players[data.id] = {};
    players[data.id].name = data.name;
    players[data.id].totalScore = data.totalScore;
    draw();
}

function handleJoinMessage(message) {
    var data = message.data;
    players[data.id] = {
        name: data.name,
        totalScore: 0
    };
    draw();
}

function draw() {

    //BG
    ctx.backgroundColor = "black";
    ctx.clearRect(0, 0, 900, 700);

    ctx.font = "35px Verdana";
    ctx.fillStyle = "#12221a";
    ctx.globalAlpha = 0.9;
    //ctx.fillRect(pixelWidth * 0.3, pixelHeight * 0.1, pixelWidth * 0.4, pixelHeight * 0.8);
    var pos = 0;

    var sortable = [];
    for (id in players) {
        sortable.push([players[id].name, players[id].totalScore]);
    }
    sortable.sort(function (a, b) {
        return b[1] - a[1];
    });

    for (var i = 0; i < sortable.length; i++) {
        ctx.fillStyle = "#FFFFFF";
        if (i === 0)
            ctx.fillStyle = "#55FF55";
        if (i === 1)
            ctx.fillStyle = "#88FF88";
        if (i === 2)
            ctx.fillStyle = "#BBFFBB";
        var scoreText = (i + 1) + ". " + sortable[i][0] + ":\t " + sortable[i][1];
        //var scoreTextWidth = ctx.measureText(scoreText).width;
        ctx.fillText(scoreText, 200/*pixelWidth / 2 - scoreTextWidth / 2*/, 35 + 40 * pos);
        pos++;
    }
    ctx.globalAlpha = 1.0;

}

legion.overlay.setOnChange(function (change, peerIds, serverIds) {
    var network = $("#network_status");
    var pc = $("#network_status_peerCount")[0];
    var sc = $("#network_status_server")[0];
    if (serverIds.length > 0) {
        sc.innerText = "Connected to [" + serverIds + "].";
    } else {
        sc.innerText = "Disconnected.";
    }
    if (peerIds.length == 0) {
        pc.innerText = "No peers.";
    } else {
        pc.innerText = "(" + peerIds.length + ") " + peerIds;
    }
    if (serverIds.length == 1) {
        network.removeClass("alert-success");
        network.addClass("alert-danger");
    } else {
        network.removeClass("alert-danger");
        network.addClass("alert-success");
    }
});