//Game
const BLOCK_SIZE = 36;

let map = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	[1, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 1], 
	[1, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 4, 0, 7, 0, 0, 0, 0, 0, 0, 3, 3, 0, 0, 0, 0, 7, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 1], 
	[1, 0, 0, 0, 0, 0, 5, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 4, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 3, 3, 1], 
	[1, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 1], 
	[1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
	[1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
	[1, 0, 0, 0, 9, 9, 9, 9, 9, 0, 0, 9, 9, 9, 9, 9, 0, 0, 0, 7, 7, 0, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 9, 0, 9, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 8, 0, 0, 0, 0, 0, 1], 
	[1, 0, 0, 0, 9,-1,-1,-1,-1, 9, 0, 0, 0, 9, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 9,-1, 9,-1, 9, 0, 0, 8, 0, 0, 0, 8, 0, 8, 0, 8, 0, 0, 0, 0, 1], 
	[1, 0, 0, 0, 9,-1,-1,-1,-1, 9, 0, 0, 0, 9, 0, 0, 0, 0, 7, 0, 7, 7, 7, 0, 0, 8, 8, 0, 0, 8, 0, 9,-1, 9,-1, 9,-1, 9, 0, 8, 0, 0, 0, 8, 0, 8, 0, 8, 0, 0, 0, 0, 1], 
	[1, 0, 0, 0, 9,-1,-1,-1,-1, 9, 0, 0, 0, 9, 0, 0, 0, 0, 7, 0, 7,-1, 7, 0, 0, 8, 0, 8, 0, 8, 0, 0, 9,-1, 9,-1, 9, 0, 0, 0, 8, 0, 8, 0, 0, 8, 0, 8, 0, 0, 0, 0, 1], 
	[1, 0, 0, 0, 9,-1,-1,-1,-1, 9, 0, 0, 0, 9, 0, 0, 0, 0, 7, 0, 7, 7, 0, 0, 0, 8, 0, 0, 8, 8, 0, 9,-1, 9,-1, 9,-1, 9, 0, 0, 8, 0, 8, 0, 8, 0, 0, 0, 8, 0, 0, 0, 1], 
	[1, 0, 0, 0, 9,-1,-1,-1,-1, 9, 0, 0, 0, 9, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 9,-1, 9,-1, 9, 0, 0, 0, 8, 0, 8, 0, 8, 0, 0, 0, 8, 0, 0, 0, 1], 
	[1, 0, 0, 0, 9, 9, 9, 9, 9, 0, 0, 9, 9, 9, 9, 9, 0, 0, 0, 7, 7, 7, 0, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 9, 0, 9, 0, 0, 0, 0, 0, 8, 0, 0, 8, 0, 0, 0, 8, 0, 0, 0, 1], 
	[1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 1], 
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 0, 0, 0, 1], 
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 8, 8, 8, 8, 0, 0, 8, 8, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
	[1, 0, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
	[1, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 8, 8, 8, 0, 0, 0, 0, 8, 8, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 1], 
	[1, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 1], 
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 8, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 1], 
	[1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 8, 8, 8, 8, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 1], 
	[1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 1], 
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 1], 
	[1, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
	[1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 0, 1], 
	[1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], 
	[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]

var colors = ["white", "blue", "red", "#73AA05", "#3E64AC", "#57AABC", "#2E773F", "#E56744", "black", "#0080FF"];

//Renderer
var canvas = document.getElementById("canvas");
var mapHeight = map.length;
var mapWidth = map[0].length;
var pixelWidth = mapWidth * BLOCK_SIZE;    //canvas.width;
var pixelHeight = mapHeight * BLOCK_SIZE;      //canvas.height;

canvas.setAttribute("width", pixelWidth + "px");
canvas.setAttribute("height", pixelHeight + "px");

canvas.setAttribute("width", window.innerWidth + "px");
canvas.setAttribute("height", window.innerHeight + "px");

var ctx = canvas.getContext("2d");

var selected = {x: 0, y: 0};
var color = 0;

function valid(y, x) {
    return map[y] != null && map[y][x] != null;
}

const tl = [];
const tr = [];
const bl = [];
const br = [];
const tlf = [];
const trf = [];
const blf = [];
const brf = [];

const pre_comp_draw = function () {
    for (let y = 0; y < mapHeight; y += 1) {
        tl[y] = [];
        tr[y] = [];
        bl[y] = [];
        br[y] = [];
        tlf[y] = [];
        trf[y] = [];
        blf[y] = [];
        brf[y] = [];
        for (let x = 0; x < mapWidth; x += 1) {
            tl[y][x] = ((valid(y - 1, x) && map[y - 1][x] > 0) || (valid(y, x - 1) && map[y][x - 1] > 0)) ? 0 : 8;
            tr[y][x] = ((valid(y - 1, x) && map[y - 1][x] > 0) || (valid(y, x + 1) && map[y][x + 1] > 0)) ? 0 : 8;
            bl[y][x] = ((valid(y + 1, x) && map[y + 1][x] > 0) || (valid(y, x - 1) && map[y][x - 1] > 0)) ? 0 : 8;
            br[y][x] = ((valid(y + 1, x) && map[y + 1][x] > 0) || (valid(y, x + 1) && map[y][x + 1] > 0)) ? 0 : 8;
            tlf[y][x] = 0;
            trf[y][x] = 0;
            blf[y][x] = 0;
            brf[y][x] = 0;
        }
    }
};
pre_comp_draw();

canvas.addEventListener('click', function (event) {
    var gridX = pixelToGrid(event.offsetX);
    var gridY = pixelToGrid(event.offsetY);

    map[gridY][gridX] = map[gridY][gridX] === 0 ? color : 0;
    pre_comp_draw();
    draw();
}, false);

function keyDown(event) {
    switch (event.keyCode) {
        case 68: //RIGHT
            if (selected.x < mapWidth - 1)
                selected.x = selected.x + 1;
            break;
        case 65: //LEFT
            if (selected.x > 0)
                selected.x = selected.x - 1;
            break;
        case 87: //UP
            if (selected.y > 0)
                selected.y = selected.y - 1;
            break;
        case 83: //DOWN
            if (selected.y < mapHeight - 1)
                selected.y = selected.y + 1;
            break;
        case 82: //R
            color = (color + 1) % colors.length;
            break;
        case 81: //Q
            color = map[selected.y][selected.x];
        case 13: //ENTER
        case 69: //e
            map[selected.y][selected.x] = color;
            break;
        case 8: //backspace
            map[selected.y][selected.x] = 0;
    }
    draw();
}


function draw() {

    //BG
    ctx.backgroundColor = "white";
    ctx.clearRect(0, 0, pixelWidth, pixelHeight);

    //Walls
    for (var y = 0; y < mapHeight; y += 1) {
        for (var x = 0; x < mapWidth; x += 1) {
            ctx.fillStyle = colors[map[y][x]];
            ctx.lineWidth = 2;
            ctx.globalAlpha = 0.2;
            ctx.strokeStyle = 'black';
            //ctx.strokeRect(x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
            ctx.globalAlpha = 1;


            if (map[y][x] === -1) {
                ctx.fillStyle = 'grey';
            }

            if (map[y][x] > 0) {
                roundRect(ctx, x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE,
                    {tl: tl[y][x], tr: tr[y][x], br: br[y][x], bl: bl[y][x]},
                    {tl: tlf[y][x], tr: trf[y][x], br: brf[y][x], bl: blf[y][x]});
            } else {
                ctx.fillRect(x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
            }


        }
    }

    ctx.strokeStyle = colors[color];
    ctx.beginPath();
    ctx.moveTo(selected.x * BLOCK_SIZE, selected.y * BLOCK_SIZE);
    ctx.lineTo((selected.x + 1) * BLOCK_SIZE, (selected.y + 1) * BLOCK_SIZE);
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo((selected.x + 1) * BLOCK_SIZE, selected.y * BLOCK_SIZE);
    ctx.lineTo((selected.x) * BLOCK_SIZE, (selected.y + 1) * BLOCK_SIZE);
    ctx.stroke();

    ctx.lineWidth = 3;
    ctx.globalAlpha = 1;
    ctx.strokeStyle = 'red';
    ctx.strokeRect(selected.x * BLOCK_SIZE, selected.y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);

    ctx.fillStyle = colors[color];

    ctx.fillRect(selected.x * BLOCK_SIZE + BLOCK_SIZE * 0.1, selected.y * BLOCK_SIZE + BLOCK_SIZE * 0.1,
        BLOCK_SIZE * 0.8, BLOCK_SIZE * 0.8);
}

const defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};

function roundRect(ctx, x, y, width, height, radius, cornerColor) {
    if (typeof radius === 'number') {
        radius = {tl: radius, tr: radius, br: radius, bl: radius};
    } else {
        for (const side in defaultRadius) {
            radius[side] = radius[side] || defaultRadius[side];
        }
    }

    if (cornerColor != null) {
        const oldColor = ctx.fillStyle;
        ctx.fillStyle = "white";
        if (radius.tl > 0 && cornerColor.tl === 0) {
            ctx.fillRect(x, y, width / 2, height / 2);
        }
        if (radius.tr > 0 && cornerColor.tr === 0) {
            ctx.fillRect(x + width / 2, y, width / 2, height / 2);
        }
        if (radius.bl > 0 && cornerColor.bl === 0) {
            ctx.fillRect(x, y + height / 2, width / 2, height / 2);
        }
        if (radius.br > 0 && cornerColor.br === 0) {
            ctx.fillRect(x + width / 2, y + height / 2, width / 2, height / 2);
        }
        ctx.fillStyle = oldColor;
    }

    ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    ctx.closePath();
    ctx.fill();
}

function shadeColor2(color, percent) {
    var f = parseInt(color.slice(1), 16), t = percent < 0 ? 0 : 255, p = percent < 0 ? percent * -1 : percent,
        R = f >> 16, G = f >> 8 & 0x00FF, B = f & 0x0000FF;
    return "#" + (0x1000000 + (Math.round((t - R) * p) + R) * 0x10000 + (Math.round((t - G) * p) + G) * 0x100 + (Math.round((t - B) * p) + B)).toString(16).slice(1);
}

function pixelToGrid(cord) {
    return Math.floor(cord / BLOCK_SIZE);
}

function gridToCenterPixel(cord) {
    return cord * BLOCK_SIZE + BLOCK_SIZE / 2;
}

window.addEventListener("keydown", keyDown, false);

draw();
